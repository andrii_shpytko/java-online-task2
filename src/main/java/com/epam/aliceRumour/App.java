package com.epam.aliceRumour;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * @author Andrii Shpytko
 * @version 1.0
 */
public class App {
    /**
     *  is the field constant which indicates the number of simulations performed
     */
    static final int NUMBER_OF_CYCLES = 1_000_000;
    /**
     *  this constant field contains the maximum number of invited guests for one iteration
     */
    static final int MAX_NUMBER_OF_FRIENDS = 23;
    static Random random = new Random();

    /**
     * Here start point of the program
     * @param args parameters
     */
    public static void main(String[] args) {
        int allCyclesCounter = 0;
        for (int i = 0; i < NUMBER_OF_CYCLES; i++) {
            List<Friend> friends = createListOfFriends();
            int cycleCounter = 0;
            boolean shouldContinue = true;
            while (shouldContinue) {
                for (Friend friend : friends) {
                    if (friend.knows)
                        cycleCounter++;
                    if(cycleCounter == friends.size())
                        allCyclesCounter++;
                    shouldContinue = false;
                }
            }
        }
        System.out.println("All loops from 1 000 000 iteration, where all  people heard invitation: " + allCyclesCounter);
        double probability = (double)allCyclesCounter / NUMBER_OF_CYCLES * 100;
        System.out.printf("The probability:  %,.4f%%\n", probability);
    }

    /**
     * This method create and initialization List friends
     * @return our List
     */
    private static List<Friend> createListOfFriends() {
        List<Friend> friends = new LinkedList<>();
        int numberOfFriends = random.nextInt(MAX_NUMBER_OF_FRIENDS) + 2;
        for (int i = 0; i < numberOfFriends; i++) {
            friends.add(new Friend());
        }
        setFriendsWhoKnow(friends);
        return friends;
    }

    /**
     *
     * @param friends is our List friends
     */
    private static void setFriendsWhoKnow(List<Friend> friends) {
        for (Friend friend : friends) {
            friend.knows = isKnows();
        }
        friends.get(0).knows = true;
    }

    /**
     *
     * @return boolean value true or false
     */
    private static boolean isKnows() {
        int number = random.nextInt(2);
        if (number > 0)
            return true;
        return false;
    }
}
